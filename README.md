# Node articles service API

It's a REST API built with Nodejs and MongoDB to query spare parts references.

### Use

```
npm install 

cp .env.example .env

npm start
```