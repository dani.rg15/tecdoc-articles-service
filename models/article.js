const mongoose = require('mongoose');

const ArticleSchema = mongoose.Schema({
	articleNumber: 			{ "type": String, "require": true },
	shortArticleNumber:		{ "type": String, "require": true },
	supplierCode: 			{ "type": Number, "require": true },
	supplierName:			{ "type": String, "require": true },
	genArt: 				{ "type": Number, "require": true },
	description: 			{ "type": String, "require": true }
}
)

ArticleSchema.index({ "shortArticleNumber": 1 });
ArticleSchema.index({ "supplierCode": 1 });


module.exports = mongoose.model('Article', ArticleSchema);