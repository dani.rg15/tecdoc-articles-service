const Article = require('../models/article')

module.exports = {
    find: (params, callback) => {
        let query = {
        	'shortArticleNumber': params.articleNumber.toUpperCase().match(/([0-9a-zA-Z])/g).join(""),
        }

        if (params.supplierCode) {
        	query['supplierCode'] = params.supplierCode;
        }

        Article.find(query)
        	   .select({'__v': 0, '_id': 0})
        	   .exec(callback)
    }
}

