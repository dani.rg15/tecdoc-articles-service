const Joi = require('joi');
 
module.exports = {
	full: {
		query: {
		    articleNumber:     Joi.string().required(),
		    supplierCode:      Joi.number().optional()
		}
	}
};
