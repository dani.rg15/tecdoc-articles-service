const validate          = require('express-validation');
const validateArticle   = require('./validators/article');
const XML 				= require("js2xmlparser")
const ArticleController    = require('../controllers/ArticleController')

module.exports = (app) => {

    app.get('/api/articles', validate(validateArticle.full), (req, res, next) => {
        ArticleController.find(req.query, (err, articles) => {
            if (err) {
                res.status(500).json(err)
                return next()
            }
            else {
                response = {
                    'status': articles.length > 0 ? 'OK' : 'ZERO_RESULTS',
                    'articles': articles || {}
                }

                res.status(200)
                res.json(response)
                return next()
            }
        })

    })

}



