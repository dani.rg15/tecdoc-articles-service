const LineByLineReader = require('line-by-line')
const Article = require('../models/article')

require('../database').connect(require('../config/dev.json').DB_URI)

function handle() {
	lr = new LineByLineReader('../data/articles.csv', { encoding: 'utf8' });

	lr.on('error', function (err) {
		if (err) throw err
	})

	lr.on('end', function () {
		console.log('Finished')
	})

	var index = 0;
	lr.on('line', function (data) {
		var line = data.split(';')

		if (line.length == 6) {
			index++
			console.log(index + ': ' + data.toString())

			let article = new Article({
				'articleNumber': 		line[0].replace (/"/g,''),	
				'shortArticleNumber': 	line[1].replace (/"/g,'').toUpperCase(),
				'supplierName':			line[2].replace (/"/g,''),
				'supplierCode': 		line[3].replace (/"/g,''),
				'genArt': 				line[4].replace (/"/g,''),
				'description': 			line[5].replace (/"/g,'')
			})

			article.save((err) => {
				if (err) throw err
			})
		}		

	})
}

handle();


