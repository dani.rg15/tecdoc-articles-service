//--------------------------------------------
//		App Modules
//--------------------------------------------
const restify 		= require('restify')
const app 			= restify.createServer()
const restifyPlugins = require('restify-plugins')
const env 			= require('dotenv').config()
const morgan 		= require('morgan')
const config 		= require('config')
const auth 			= require('./middleware/jwtAuth')

//--------------------------------------------
//		Middlewares
//--------------------------------------------
app.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
app.use(restifyPlugins.acceptParser(app.acceptable));
app.use(restifyPlugins.queryParser({ mapParams: true }));
app.use(restifyPlugins.fullResponse());


//--------------------------------------------
//		Configuration
//--------------------------------------------
const port = process.env.PORT || 3000

if(config.util.getEnv('NODE_ENV') !== 'test') {
    app.use(morgan('dev'))
}

// const log_strem = require('fs').createWriteStream('./logs/access.log', {flags: 'a'})
// app.use(morgan('tiny', {stream: log_strem}))

//-------------------------------------------
//		DB connection
//--------------------------------------------
require('./database').connect(config.DB_URI)


//--------------------------------------------
//		Routing
//--------------------------------------------
//app.use(auth)
require('./routes/article.routes')(app)

//--------------------------------------------
//		Runnn!
//--------------------------------------------
app.listen(port, function(err) {
	if (err) throw err
	console.log('Server running on port: ' + port)
})

// require('./auth').getNewToken({
// 	'issuer': 'GAUIb',
// 	'expiresIn': '90 days'
// }, {
// 	'user': '*'
// }, (err, token) => {
// 	if (err) {
// 		throw err
// 	}
// 	else{
// 		console.log(token)
// 	} 
// })




